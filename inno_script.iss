﻿#define BinDir "bin\Debug-windows-x86_64\Demo"
#define ExeName "Demo"

[Setup]
AppName=FrasyApp
AppVersion=1.0
AppId=FB184580-7A2F-428E-9631-89A14A6487D0
AppPublisher=Conception Électronique Privé, Inc.
AppPublisherURL=www.cep-inc.ca                                                                                          
AppSupportPhone=+1-581-983-2873      
AppContact=support@cep-inc.ca
AppCopyright=Conception Électronique Privé, Inc. © 2012-2024
AppReadmeFile=https://gitlab.com/conception-electronique-prive/templates/frasy-application/-/blob/master/README.md
AppSupportURL=https://gitlab.com/conception-electronique-prive/templates/frasy-application/-/issues
AppUpdatesURL=https://gitlab.com/conception-electronique-prive/templates/frasy-application/-/releases

WizardStyle=modern
DefaultDirName={autopf}\FrasyApp
DefaultGroupName=FrasyApp
AllowUNCPath=no
AlwaysShowComponentsList=no
PrivilegesRequired=lowest
OutputBaseFilename="FrasyAppInstaller"
OutputDir="{#BinDir}\.."

[Files]
Source: "{#BinDir}\{#ExeName}.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#BinDir}\{#ExeName}.pdb"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#BinDir}\assets\*"; DestDir: "{app}"; Flags: recursesubdirs createallsubdirs ignoreversion
Source: "{#BinDir}\lua\*"; DestDir: "{app}"; Flags: recursesubdirs createallsubdirs ignoreversion
Source: "bin\VC_redist.x64.exe"; DestDir: "{tmp}"; Flags: deleteafterinstall

[Icons]
Name: "{userstartmenu}\FrasyApp"; Filename:"{app}\Demo.exe"; IconFilename: "{app}\assets\textures\icon.ico"; WorkingDir: "{app}"
Name: "{userdesktop}\FrasyApp"; Filename:"{app}\Demo.exe"; IconFilename: "{app}\assets\textures\icon.ico"; WorkingDir: "{app}"

[Run]
Filename: "{tmp}\VC_redist.x64.exe"; Parameters: "/install /quiet /norestart"; StatusMsg: "Installing Microsoft Visual C++ Redistributable..." 
