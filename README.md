# Frasy Application

Template for making a Frasy application


## Usage

cpp files in src are automatically added to project.

You can added more files if needed by changing premake5.lua files at line 38

```
files {
   "src/**.cpp",
   -- your files
}
```


You can generate your solution by invoking the following command
`.\vendor\Frasy\vendor\bin\premake\premake5.exe <ide>` where ide can be `clion`, `frasy`